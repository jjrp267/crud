<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Login_m extends CI_Model {
  public function __construct(){parent::__construct();}
    
  function no_existe_cuenta($sEmail,$sPass='',$opc=1){
    
    if (empty($sEmail) or empty($opc))
      return true;// no se puede validar
        
    if ($opc==1){
      // armamos la consulta
      $query = $this->db
      ->query('SELECT email FROM usuarios WHERE email=?',array($sEmail)); 
    }
    elseif ($opc==2){
      // encriptamos la contraseña
      if (!empty($sPass)) $sPass=md5($sPass);
        
      // armamos la consulta
      $query = $this->db->query(
      'SELECT email,password FROM usuarios WHERE email=? AND password=?',
      array($sEmail,$sPass));
    }
    return ($query->num_rows()) ? false : true;
  }
}
?>
