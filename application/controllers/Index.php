<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Index extends CI_Controller {
  public function __construct(){parent::__construct();}

  public function index(){
    $arrSesion = $this->session->userdata('ses_usuario');
    
    // cerramos la sesion si el tiempo establecido expiro
    if ($arrSesion['seslimite']<=time())
        $this->cerrar_sesion();
    
    $datos['sTitulo'] = 'CodeIgniter: Index';
    
    // esto es opcional, nos calcula el tiempo restante de la sesion
    $datos['TiempoRestante'] = $arrSesion['seslimite'] ? 
    ('quedan ' .($arrSesion['seslimite'] - time()) .' segundos') : '';
    
    // verificamos la sesión 
    $datos['sSaludo'] = $this->session->userdata('ses_usuario') ? 
        'Bienvenido <b>'. $arrSesion['email'] . '</b> | <a href="'.
        site_url('index/cerrar_sesion'). '">Cerrar sesión</a>' : 
        'Bienvenido <b>visitante</b>, por favor <a href="'.
        site_url('login').'">Inicia sesión</a>';
    
    // cargamos  la interfaz
    $this->load->view('index', $datos);
  }
  
  public function cerrar_sesion($sSesion='ses_usuario')  {  
    // cerramos la sesión
    $this->session->unset_userdata($sSesion);
    //$this->session->sess_destroy();
      
    // redireccionamos al formulario de login
    redirect('login', 'location'); 
  }
}
?>
