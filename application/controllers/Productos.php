<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/* Heredamos de la clase CI_Controller */
class Productos extends CI_Controller {

  function __construct()
  {

    parent::__construct();

    /* Cargamos la base de datos */
    $this->load->database();

    /* Cargamos la libreria*/
    $this->load->library('grocery_crud');

    /* Añadimos el helper al controlador */
    $this->load->helper('url');
  }

  function index()
  {
    /*
     * Mandamos todo lo que llegue a la funcion
     * administracion().
     **/
    redirect('productos/administracion');
  }

  /*
   *
   **/
  function administracion()
  {
    try{

        
    /* Creamos el objeto */
    $crud = new Grocery_CRUD();

    /* Seleccionamos el tema */
    //$crud->set_theme('flexigrid');
    //$crud->set_theme('twitter-bootstrap');
    $crud->set_theme('datatables');

    /* Seleccionmos el nombre de la tabla de nuestra base de datos*/
    $crud->set_table('productos');

    /* Le asignamos un nombre */
    $crud->set_subject('Productos');
    
    if(!$this->session->userdata('ses_usuario'))
    {    
        $crud->unset_add();
        $crud->unset_edit();
        $crud->unset_delete();
    }
        
    /* Asignamos el idioma español */
    $crud->set_language('spanish');

    /* Aqui le decimos a grocery que estos campos son obligatorios */
    $crud->required_fields(
      'id',
      'nombre',
      'descripcion',
      'precio_venta',
      'existencia'
    );

    /* Aqui le indicamos que campos deseamos mostrar */
    $crud->columns(
      'id',
      'proveedor',
      'nombre',
      'descripcion',
      'precio_compra',
      'precio_venta',
      'existencia'
    );

    /* Generamos la tabla */
    $output = $crud->render();

    //$data['tabla']= $output;
    //$data['usuario']=$this->session->userdata('ses_usuario');     
    
    log_message('error', 'alguna_variable no contenía valorrrrr.');
    
    /* La cargamos en la vista situada en
    /applications/views/productos/administracion.php */
    $this->load->view('productos/administracion', $output);

    }catch(Exception $e){
      /* Si algo sale mal cachamos el error y lo mostramos */
      show_error($e->getMessage().' --- '.$e->getTraceAsString());
    }
  }
  
  public function obtener_sesion($sSesion='ses_usuario')  {  
      //Obtener sesion    
      return $this->session->userdata($sSesion);
       
    }  
  
}
