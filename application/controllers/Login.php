<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Login extends CI_Controller {

  // nuestro metodo costructor
  // primer metodo a ejecutarse dentro de la clase
  public function __construct(){
    // para que pueda heredar lo que esta en el controlador padre
    parent::__construct();

    // cargamos el "helper form", necesario si queremos
    // armar el formulario directamente con codeigniter
    $this->load->helper('form');

    // cargamos la libreria de validacion de fornmulario
    $this->load->library('form_validation');
      
    // cargamos el modelo del login
    $this->load->model('login_m');
  }

  public function index(){  
    // si la sesion existe, redireccionamos a la pagina principal
    if($this->session->userdata('ses_usuario'))
      redirect('informacion', 'location'); 
    else
      // sino, cargamos el formulario de login  
      $this->_cargar_login_view();
  }
  
  // gestiona lo que viene del formulario
  public function validar_form()  {
    //print_r($_POST);exit;  
      
    // armamos un array con las reglas de validación
    $arrValidaciones = array(
      array(
        'field'   => 'txtEmail',
        'label'   => 'Email',
        'rules'   => 'required|min_length[10]|max_length[50]
        |valid_email|callback__existe_email'
      ),
      array(
        'field'   => 'txtPassword',
        'label'   => 'contraseña',
        // encriptamos al recibir
        'rules'   => 'required|min_length[6]|max_length[25]|md5'
      ),
      array(
        'field'   => 'txtSesLimite',
        'label'   => 'duración de la sesión',
        'rules'   => 'required|min_length[1]|max_length[3]|integer'
      )
    );
    // establecemos las reglas de validacion
    $this->form_validation->set_rules($arrValidaciones);

    // indicamos que los errores se les aplique la clase form_error (CSS)
    $this->form_validation
    ->set_error_delimiters('<div class="div_error">* ','</div>');

    // establecemos un mensaje para el caso de email incorrecto 
    $this->form_validation
    ->set_message('_existe_email', 'El <b>Email</b> introducido no existe');

    $sEmail    = $this->input->post('txtEmail');
    $sPassword = $this->input->post('txtPassword');
    $SesLimite = $this->input->post('txtSesLimite');
    
    // iniciamos las validaciones
    if ($this->form_validation->run() == FALSE)
      // si hubo algún error, retornamos al formulario
      $this->_cargar_login_view();

    // nos aseguramos que coincidan email y contraseña
    elseif ($this->login_m->no_existe_cuenta($sEmail,$sPassword,2))

      // cargamos de nuevo la vista, pero pasandole el error producido
      $this->_cargar_login_view('El <b>Email</b> y la <b>contraseña</b>
      no coinciden');
    else
      // generamos la sesion
      $this->_generar_sesion($sEmail,$SesLimite);
  }
  
  function _cargar_login_view($sMsjError=''){
    // pasamos el titulo
    $datos['sTitulo'] = 'Acceso: Login';
      
    // en caso del mensaje personalizado de error, lo enviamos
    if (!empty($sMsjError))
      $datos['sMsjError'] = $sMsjError;
      
    // cargamos  la interfaz
    $this->load->view('seguridad/login', $datos);
  }
  
  // verificamos la existencia del email
  function _existe_email($sEmail){
    return ($this->login_m->no_existe_cuenta($sEmail)) ? 
    false : true;
  }
  
  // generamos la sesion con los datos del usuario
  function _generar_sesion($sEmail='[desconocido]',$SesLimite=1){
    // armamos un array con los datos de la sesion
    $arrSesion = array(
      'is_logged' => true,
      'email'     => $sEmail,
      'seslimite' => time() + ($SesLimite * 60)// limite en minutos
    );
    
    // se establece la sesion
    $this->session->set_userdata('ses_usuario',$arrSesion);
    
    // redireccionamos a la pagina principal
    redirect('informacion', 'location');
  }
  
    public function cerrar_sesion($sSesion='ses_usuario')  {  
    // cerramos la sesión
    $this->session->unset_userdata($sSesion);
    //$this->session->sess_destroy();
      
    // redireccionamos al formulario de login
    redirect('informacion', 'location'); 
  }
  
    public function obtener_sesion($sSesion='ses_usuario')  {  
      //Obtener sesion    
      $this->session->userdata($sSesion);
      redirect('productos', 'location'); 
    }  
  
}
?>
