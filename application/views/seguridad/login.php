<!DOCTYPE html>
<html>
 <div id = "header">
      <?php $this->load->view('plantillas/header');?>
</div>
 <body>
   <div id = "navigation">
        <?php $this->load->view('plantillas/navigation');?>          
    </div>
  <div id='div_formulario'>
      <?php $this->load->view('seguridad/formulario');?>  
  </div>
  <div id = "footer">
       <?php $this->load->view('plantillas/footer');?>
  </div>     
 </body>
</html>
