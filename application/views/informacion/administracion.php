<!DOCTYPE html>
<html>
    <div id = "header">
        <?php $this->load->view('plantillas/header');?>
        <?php foreach($css_files as $file): ?>
            <link type="text/css" rel="stylesheet" href="<?php echo $file; ?>" />
        <?php endforeach; ?>
        <?php foreach($js_files as $file): ?>
            <script src="<?php echo $file; ?>"></script>
        <?php endforeach; ?>
    </div>
    <body>
        <div id = "navigation">
        <?php $this->load->view('plantillas/navigation');?>          
        </div>    
        <div>            
              <?php echo $output; ?>            
        </div> 
        <div id = "footer">
              <?php $this->load->view('plantillas/footer');?>
        </div>
    </body>
</html>
