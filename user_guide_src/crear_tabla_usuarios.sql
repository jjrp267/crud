CREATE TABLE IF NOT EXISTS tipo_usuario (
  id_tipo_usuario TINYINT NOT NULL AUTO_INCREMENT PRIMARY KEY,
  nombre VARCHAR(25) NOT NULL,
  descripcion VARCHAR(100) NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8; 

CREATE TABLE IF NOT EXISTS usuarios 
(
   id_usuario INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
   id_tipo_usuario TINYINT NOT NULL,
   nombre VARCHAR(25) NOT NULL,
   email VARCHAR(50) NOT NULL,
   password VARCHAR(32) NOT NULL,
   codigov CHAR(20) NOT NULL,
   activa TINYINT NOT NULL DEFAULT 0,
   FOREIGN KEY (id_tipo_usuario) 
   REFERENCES tipo_usuario(id_tipo_usuario)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO tipo_usuario (nombre,descripcion) VALUES 
('administrador','usuario de nivel 1 - todos los privilegios');

-- registro de ejemplo, password:12345678
INSERT INTO usuarios (id_tipo_usuario,nombre,email,password,codigov,activa) VALUES 
(1,'user','id@hotmail.com','25d55ad283aa400af464c76d713c07ad','25aa400af467d55ad283',1);