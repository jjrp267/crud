delimiter $$

CREATE TABLE `productos` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '\n    id tipo entero (debe ser llave primaria y autoincrementable)\n    proveedor tipo varchar con una longitud de 50 caracteres.\n    nombre tipo varchar con una longitud de 128 caracteres.\n    descripcion tipo varchar con una longitud de 250 caracteres.\n    precio_compra tipo float\n    precio venta tipo float\n    exitencia tipo int\n',
  `proveedor` varchar(50) DEFAULT NULL,
  `nombre` varchar(128) DEFAULT NULL,
  `descripcion` varchar(250) DEFAULT NULL,
  `precio_compra` float DEFAULT NULL,
  `precio_venta` float DEFAULT NULL,
  `existencia` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1$$

